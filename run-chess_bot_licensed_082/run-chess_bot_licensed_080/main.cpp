#include <windows.h>
#include <TlHelp32.h>
#include <stdio.h>

unsigned long get_module(unsigned long pid, char *module_name, unsigned long *size = 0) {
	void *snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);
	MODULEENTRY32 me32;
	me32.dwSize = sizeof(MODULEENTRY32);

	while (Module32Next(snapshot, &me32)) {
		if (strcmp(me32.szModule, module_name) == 0) {
			if (size != 0) *size = me32.modBaseSize;
				return (unsigned long)me32.modBaseAddr;
		}
	} return NULL;
}

int CALLBACK WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	STARTUPINFO si = { sizeof(STARTUPINFO) };
    si.cb = sizeof(si);
    si.dwFlags = STARTF_USESHOWWINDOW;
    si.wShowWindow = SW_SHOW;
    PROCESS_INFORMATION pi;
    CreateProcess("bin/ChessBot.exe", NULL , NULL, NULL, FALSE, CREATE_NO_WINDOW , NULL, NULL, &si, &pi);

	if (pi.hProcess) {
		unsigned long base;
    unsigned long oldProtect;
    void *hwnd = 0;

    while (hwnd == 0) {
      hwnd = FindWindowA(0, "ChessBot 0.82 [TRIAL]");
    }

    SetWindowTextA((HWND)hwnd, "ChessBot 0.82 [MODDED BY LAVA]");
    base = get_module(pi.dwProcessId, "ChessBot.exe");


    // 00658B00    83C0 7F         ADD EAX,7F
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x00658B02 - 0x00400000),
                       "\x7F", 1, 0);
    
    // 00658B05   /EB 43           JMP SHORT ChessBot.00658B4A
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x00658B05 - 0x00400000),
                       "\xEB", 1, 0);
    
    // 006583B2    83C0 07         ADD EAX,7
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x006583B4 - 0x00400000),
                        "\x7F", 1, 0);

    // 006583BB   /7D 39           JGE SHORT ChessBot.006583F6
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x006583BB - 0x00400000),
                       "\xEB", 1, 0);

    // 00656FCD    83C0 07         ADD EAX,7
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x00656FCF - 0x00400000),
                       "\x7F", 1, 0);

    // 00656FD6   /7D 0D           JGE SHORT ChessBot.00656FE5
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x00656FD6 - 0x00400000),
                       "\xEB", 1, 0);

    // 0065848F    83F8 0A         CMP EAX,0A
    WriteProcessMemory(pi.hProcess,
                       (void *)(base + 0x0065848F + 2 - 0x00400000), "\x7F", 1,
                       0);

    // 00658492   /7F 10           JG SHORT ChessBot.006584A4
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x00658492 - 0x00400000),
                       "\x90\x90", 2, 0);

    // 0064FDA5    83F8 14         CMP EAX,14
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064FDA7 - 0x00400000),
                       "\x7F", 1, 0);

    // 0064FDA8   /75 1F           JNZ SHORT ChessBot.0064FDC9
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064FDA8 - 0x00400000),
                       "\xEB", 1, 0);

    // 00659638    83C0 02         ADD EAX,2
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0065963A - 0x00400000),
                       "\x7F", 1, 0);

    // 00659641   /7D 05           JGE SHORT ChessBot.00659648
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x00659641 - 0x00400000),
                       "\xEB", 1, 0);


    // 0064F8C2    83C0 0A         ADD EAX,0A
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064F8C4 - 0x00400000),
                       "\x7F", 1, 0);

    //0064F8CB   /7D 05           JGE SHORT ChessBot.0064F8D2
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064F8CB - 0x00400000),
                       "\xEB", 1, 0);

    // 0064EC89    83F8 10         CMP EAX,10
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064EC8B - 0x00400000),
                       "\x7F", 1, 0);

    // 0064EC8C   /7E 10           JLE SHORT ChessBot.0064EC9E
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064EC8C - 0x00400000),
                       "\xEB", 1, 0);

    // 0064C906    83F8 11         CMP EAX,11
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064C908 - 0x00400000),
                       "\x7F", 1, 0);

    // 0064C909   /7F 11           JG SHORT ChessBot.0064C91C
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064C909 - 0x00400000),
                       "\x90\x90", 2, 0);

    // 0064F9A2    833D 5C576600 11      CMP DWORD PTR DS:[66575C],11
    WriteProcessMemory(pi.hProcess,
                       (void *)(base + 0x0064F9A2 + 6 - 0x00400000), "\x7F", 1,
                       0);

    // 0064F9A9   /7F 0D                 JG SHORT ChessBot.0064F9B8
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064F9A9 - 0x00400000),
                       "\x90\x90", 2, 0);

    // 0064EA0A    833D 5C576600 10      CMP DWORD PTR DS:[66575C],10
    WriteProcessMemory(pi.hProcess,
                       (void *)(base + 0x0064EA0A + 6 - 0x00400000), "\x7F", 1,
                       0);

    // 0064EA11   /7F 0F                 JG SHORT ChessBot.0064EA22
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064EA11 - 0x00400000),
                       "\x90\x90", 2, 0);

    // 0064E5C0    833D 5C576600 11      CMP DWORD PTR DS:[66575C],11
    WriteProcessMemory(pi.hProcess,
                       (void *)(base + 0x0064E5C0 + 6 - 0x00400000), "\x7F", 1,
                       0);

    // 0064E5C7   /7E 10                 JLE SHORT ChessBot.0064E5D9
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064E5C7 - 0x00400000),
                       "\xEB", 1, 0);

    // 0064FD50    833D 64576600 19      CMP DWORD PTR DS:[665764],19
    WriteProcessMemory(pi.hProcess,
                       (void *)(base + 0x0064FD50 + 6 - 0x00400000), "\x7F", 1,
                       0);

    // 0064FD57   /7D 42                 JGE SHORT ChessBot.0064FD9B
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064FD57 - 0x00400000),
                       "\x90\x90", 2, 0);

    // 0064E9CA    833D 58576600 11      CMP DWORD PTR DS:[665758],11
    WriteProcessMemory(pi.hProcess,
                       (void *)(base + 0x0064E9CA + 6 - 0x00400000), "\x7F", 1,
                       0);

    // 0064E9D1   /7F 27                 JG SHORT ChessBot.0064E9FA
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064E9D1 - 0x00400000),
                       "\x90\x90", 2, 0);

    // 0065182C    833D 58576600 11      CMP DWORD PTR DS:[665758],11
    WriteProcessMemory(pi.hProcess,
                       (void *)(base + 0x0065182C + 6 - 0x00400000), "\x7F", 1,
                       0);

    // 00651833   /0F8F AF020000         JG ChessBot.00651AE8
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x00651833 - 0x00400000),
                       "\x90\x90\x90\x90\x90\x90", 6, 0);

    // 0065852B    833D 64576600 07      CMP DWORD PTR DS:[665764],7
    WriteProcessMemory(pi.hProcess,
                       (void *)(base + 0x0065852B + 6 - 0x00400000), "\x7F", 1,
                       0);

    // 00658532  ^ 0F8C CBFCFFFF         JL ChessBot.00658203
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x00658532 - 0x00400000),
                       "\x90\xE9", 2, 0);

    // 0065AA40    833D 58576600 08      CMP DWORD PTR DS:[665758],8
    WriteProcessMemory(pi.hProcess,
                       (void *)(base + 0x0065AA40 + 6 - 0x00400000), "\x7F", 1,
                       0);

    // 0065AA47  ^ 0F8C A8EBFFFF         JL ChessBot.006595F5
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0065AA47 - 0x00400000),
                       "\x90\xE9", 2, 0);

    // 00650DA2    833D 58576600 10      CMP DWORD PTR DS:[665758],10
    WriteProcessMemory(pi.hProcess,
                       (void *)(base + 0x00650DA2 + 6 - 0x00400000), "\x7F", 1,
                       0);

    // 00650DA9  ^ 0F8C D0EAFFFF         JL ChessBot.0064F87F
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x00650DA9 - 0x00400000),
                       "\x90\xE9", 2, 0);

    // 00659865    83C0 05               ADD EAX,5
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x00659867 - 0x00400000),
                       "\x7F", 1, 0);

    // 0065986E   /7D 40                 JGE SHORT ChessBot.006598B0
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0065986E - 0x00400000),
                       "\xEB", 1, 0);

    // 00659687    83C0 05               ADD EAX,5
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x00659689 - 0x00400000),
                       "\x7F", 1, 0);

    // 00659690   /7D 40                 JGE SHORT ChessBot.006596D2
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x00659690 - 0x00400000),
                       "\xEB", 1, 0);

    // 006582DF    83C0 05               ADD EAX,5
    WriteProcessMemory(pi.hProcess,
                       (void *)(base + 0x006582DF + 2 - 0x00400000), "\x7F", 1,
                       0);

    // 006582E8   /7D 39                 JGE SHORT ChessBot.00658323
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x006582E8 - 0x00400000),
                       "\xEB", 1, 0);

    // 0064FB88    83C0 0C               ADD EAX,0C
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064FB8A - 0x00400000),
                       "\x7F", 1, 0);

    // 0064FB91   /7D 40                 JGE SHORT ChessBot.0064FBD3
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064FB91 - 0x00400000),
                       "\xEB", 1, 0);

    // 0064FAF7    83C0 0F               ADD EAX,0F
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064FAF9 - 0x00400000),
                       "\x7F", 1, 0);

    // 0064FB00   /0F8C 93120000         JL ChessBot.00650D99
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064FB00 - 0x00400000),
                       "\x90\x90\x90\x90\x90\x90", 6, 0);

    // 0064FAA7    83C0 10               ADD EAX,10
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064FAA9 - 0x00400000),
                       "\x7F", 1, 0);

    // 0064FAB0   /7D 02                 JGE SHORT ChessBot.0064FAB4
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064FAB0 - 0x00400000),
                       "\xEB", 1, 0);

    // 0064F9D6    83C0 10               ADD EAX,10
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064F9D8 - 0x00400000),
                       "\x7F", 1, 0);

    // 0064F9DF   /75 17                 JNZ SHORT ChessBot.0064F9F8
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064F9DF - 0x00400000),
                       "\xEB", 1, 0);

    // 0064F911    83C0 0C               ADD EAX,0C
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064F913 - 0x00400000),
                       "\x7F", 1, 0);

    // 0064F91A   /7D 40                 JGE SHORT ChessBot.0064F95C
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064F91A - 0x00400000),
                       "\xEB", 1, 0);

    // 0064C937    83C0 12               ADD EAX,12
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064C939 - 0x00400000),
                       "\x7F", 1, 0);

    // 0064C93D   /7F 11                 JG SHORT ChessBot.0064C950
    WriteProcessMemory(pi.hProcess, (void *)(base + 0x0064C93D - 0x00400000),
                       "\x90\x90", 2, 0);
	}

	return 0;
}